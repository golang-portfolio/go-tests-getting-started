go test -v -run=Test_SayGoodBye

go test -cover

go test -coverprofile=cover_out

go tool cover -html=cover_out -o cover_out.html

go test -bench=.

go test -bench=BechmarkSayHello